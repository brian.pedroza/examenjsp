<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
	String contextPath= request.getContextPath();
	%>
    <%@ page isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ page import="com.softtek.academy.javaweb.dao.ActivityDao"%>
    <%@ page import="com.softtek.academy.javaweb.bean.Activities" %>
    <%@page import="java.util.ArrayList" %>
    <%@page import="java.util.List" %> 
    <%
    	List<Activities> act = new ArrayList<>(); 
	    ActivityDao dao = new ActivityDao();
	    act = dao.getAllActivities();
	    System.out.println("act"+act);

		request.setAttribute("alist", act);
    %>
       
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
     <a href="<%=contextPath%>/index.jsp"> BACK TO MENU</a>
	<table border="2" width="70%" cellpadding="2">
         <tr>
         	<th>Id</th>
            <th>List</th>
            <th>IS DONE</th>
         </tr>
          <c:forEach items="${alist}" var="v">
              <tr>
              	<c:if test="${v.is_done}">
                  <td>${v.id}</td>
                  <td>${v.list}</td>
                  <td>${v.is_done}</td> 
                 </c:if>
               </tr>
          </c:forEach>
     </table> 
     


</body>
</html>